## ProjectX Training
For a fresh start, consider running these commands 
* `rimraf node_modules` 
* `npm install`

<a href="https://github.com/RajuKottedi/ProjectXTraining/blob/master/README.md">Readme</a>

<a name="2017-07-07"></a>
# 2017-07.07
* Added ProjectX with MSTest

<a name="2017-07-08"></a>
# 2017-07-08
* Development Setup Ready

<a name="2017-07-09"></a>
# 2017-07-09
* Changed Solution Folder structure  (Multiple Projects)

<a name="2017-07-10"></a>
# 2017-07-10
* Added support to run on VS2017 and VSCode. 
* Added Tasks to automate the build process and moved tasks to Root Folder.
* Added shortcuts to Tasks.
* Build     - shift+b
* Watch     - shift+w
* Restore   - shift+r
* Run       - shift+a

* Added DBScripts File

<a name="2017-07-14"></a>
# 2017-07-14
* Added webpack tools. (can add sass files in styleUrls)

<a name="2017-07-17"></a>
# 2017-07-17
* Added Codelyzer
    
