﻿// <copyright file="Startup.cs" company="ProjectX">
// Copyright (c) ProjectX. All rights reserved.
// </copyright>

namespace ProjectX
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using ProjectX.Core;

    public class Startup : CoreStartup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Startup(env, builder.Build());
        }
    }
}
