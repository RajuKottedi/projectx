﻿// <copyright file="HomeController.cs" company="ProjectX">
// Copyright (c) ProjectX. All rights reserved.
// </copyright>

namespace ProjectX.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        // GET: /<controller>/
        [AllowAnonymous]
        public IActionResult Index()
        {
            return this.View();
        }
    }
}
