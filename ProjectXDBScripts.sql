-- Create Database ProjectXDB

--create table test (id int Primary Key, name nvarchar(50), Description nvarchar(500))

CREATE TABLE Persons (
    PersonID int Not Null,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255) ,
	CONSTRAINT PK_Person Primary Key (PersonID, LastName)
);
